#include <stdio.h>

int step0 = 0, step1 = 0;

int state_machine_0()
{
  if(step0 == 0){
    printf("He");
    step0 = 1;
    return step0;
  }
  if(step0 == 1){
    printf("o,");
    step0 = 2;
    return step0;
  }
  if(step0 == 2){
    printf("or");
    step0 = 99;
  }
  return step0;
}

int state_machine_1()
{
  if(step1 == 0){
    printf("ll");
    step1 = 1;
    return step1;
  }
  if(step1 == 1){
    printf(" W");
    step1 = 2;
    return step1;
  }
  if(step1 == 2){
    printf("ld!\n");
    step1 = 99;
  }
  return step1;
}

int main() {
  do{
    state_machine_0();
    state_machine_1();
  }
  while(step0 != 99 && step1 != 99);
}